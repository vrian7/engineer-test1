<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Generator;
use Exception;
use Cache;
use App\ProcessMaker\ProcessMaker;
class UsersController extends Controller
{
	public function index(Request $request, Generator $faker) {


		// $list = [];
		// for($i=0; $i < 100; $i++) {
		// 	$list[]=[
		//         "usr_username" => $faker->name,
		//         "usr_email" => $faker->unique()->safeEmail,
		//         "usr_create_date" => $faker->date,
		//     ];
	 //    }
	
		$users = $this->getUsers();
		$list	= $users;  
	    return ["data" => $list];
	}

	private function getUsers(){
		
		$process_maker = new ProcessMaker();
		$accessToken = isset($_COOKIE['access_token']) ? $_COOKIE['access_token'] : $process_maker->getAccessToken();
		$apiServer = "http://10.100.64.5:8082";

		$ch = curl_init($apiServer . "/api/1.0/workflow/users");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$aUsers = json_decode(curl_exec($ch));
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$aActiveUsers = array();

		if ($statusCode == 200)
			return $aUsers;		 
		else 
			print "Error: HTTP status code: $statusCode\n";
		// if ($statusCode != 200) {
		   
		// }
		// else {
		// 	return $aUsers;		  
		// }
	}

	public function approve() {
		$process_maker = new ProcessMaker();		
		$accessToken = isset($_COOKIE['access_token']) ? $_COOKIE['access_token'] : $process_maker->getAccessToken();
		$apiServer = "http://10.100.64.5:8082";

		$ch = curl_init($apiServer . "/api/1.0/workflow/plugin-test1/pass");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch, CURLOPT_POST, 1);

		$result = json_decode(curl_exec($ch));
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
		curl_close($ch);
	

		if ($statusCode != 200) {
		   if (isset ($aUsers) and isset($aUsers->error))
		      print "Error code: {$aUsers->error->code}\nMessage: {$aUsers->error->message}\n";
		   else
		      print "Error: HTTP status code: $statusCode\n";
		}
		else {
			return "Success";		   
		}
	}
}
