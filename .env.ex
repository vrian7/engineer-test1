APP_ENV=local
APP_KEY=base64:3r54cj+iivVtdxpDHaPJ9T+F7OgRJBwvl3rGRLsINSM=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=sqlite

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=

PROCESSMAKER_PORT=8082
PROCESSMAKER_URL=http://10.100.64.5:8082
PROCESSMAKER_USER=admin
PROCESSMAKER_PASSWORD=admin